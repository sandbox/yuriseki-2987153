<?php

namespace Drupal\yutils;

// TODO put each set of functions in its own class such as Fields, Requests...
// Use  the service container and Yutil needs to became only the api.
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\file\Entity\File;

/**
 * Util functions used by me.
 *
 * @package Drupal\yutil
 */
class Yutil {

  /**
   * Get the field setting of an entity in a view mode.
   *
   * @param string $entity_type
   * @param string $bundle
   * @param string $view_mode
   * @param string $field_name
   *
   * @return array
   */
  public static function getFieldSettings($entity_type, $bundle, $view_mode, $field_name) {
    $settings = \Drupal::service('entity_type.manager')
      ->getStorage('entity_view_display')
      ->load($entity_type . '.' . $bundle . '.' . $view_mode)
      ->getRenderer($field_name)
      ->getSettings();

    return $settings;
  }

  /**
   * Return the URL for an image with style applied.
   *
   * @param \Drupal\file\Entity\File $file
   * @param $style_name
   *
   * @return string
   *   The Url for the stylized image.
   */
  public static function getStylizedImageUrl(File $file, $style_name) {
    $style         = \Drupal::entityTypeManager()
      ->getStorage('image_style')
      ->load($style_name);
    if ($file instanceof File) {
      $image_nav_url = $style->buildUrl($file->getFileUri());
    }
    else {
      $image_nav_url = '';
    }

    return $image_nav_url;
  }

  /**
   * @param string $method
   * @param string $url
   * @param array $headers
   * @param string $content
   *
   * @return mixed
   *
   * @throws \RuntimeException
   */
  public static function sendJasonViaRequest($method, $url, array $headers, $content = NULL) {

    // TODO include usage example.

    $headers[] = 'Accept: application/json';
    $headers[] = 'Accept-Encoding: gzip';
    $headers[] = 'RequestId: ' . uniqid();

    $curl = curl_init($url);

    curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);

    $method = strtoupper($method);
    switch ($method) {
      case 'GET':
        break;
      case 'POST':
        curl_setopt($curl, CURLOPT_POST, TRUE);
        break;
      default:
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
    }

    if ($content !== NULL) {
      curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

      $headers[] = 'Content-Type: application/json';
    }
    else {
      $headers[] = 'Content-Length: 0';
    }

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

    $response   = curl_exec($curl);
    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if (curl_errno($curl)) {
      throw new \RuntimeException('Curl error: ' . curl_error($curl));
    }

    curl_close($curl);

    return [
      'status_code' => $statusCode,
      'response' => json_decode($response),
    ];
  }
}
